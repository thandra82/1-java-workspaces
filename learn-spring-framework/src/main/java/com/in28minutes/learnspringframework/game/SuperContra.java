package com.in28minutes.learnspringframework.game;

import org.springframework.stereotype.Component;

@Component
public class SuperContra implements GamingConsole{

    public void up(){
        System.out.println("SuperContra please jump");
    }

    public void down(){
        System.out.println("SuperContra please sleep");
    }

    public void left(){
        System.out.println("SuperContra go left");
    }

    public void right(){
        System.out.println("SuperContra go right");
    }
}
