package com.in28minutes.learnspringframework.game;

import org.springframework.stereotype.Component;

@Component
public class MarioGame implements GamingConsole {

    public void up(){
        System.out.println("Mario please jump");
    }

    public void down(){
        System.out.println("Mario please sleep");
    }

    public void left(){
        System.out.println("Mario go left");
    }

    public void right(){
        System.out.println("Mario go right");
    }
}
