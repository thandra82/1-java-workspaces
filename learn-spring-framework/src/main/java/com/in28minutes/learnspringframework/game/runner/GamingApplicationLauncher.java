package com.in28minutes.learnspringframework.game.runner;

import com.in28minutes.learnspringframework.game.GameRunner;
import com.in28minutes.learnspringframework.game.GamingConsole;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.in28minutes.learnspringframework.game")
public class GamingApplicationLauncher {
    public static void main(String[] args) {
        try(var context = new AnnotationConfigApplicationContext(GamingApplicationLauncher.class)){

            GamingConsole consoleApp = context.getBean(GamingConsole.class);

            consoleApp.down();

            GameRunner gameRunnerApp = context.getBean(GameRunner.class);

            gameRunnerApp.run();

        }
    }
}
