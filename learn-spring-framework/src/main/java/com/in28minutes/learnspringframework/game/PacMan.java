package com.in28minutes.learnspringframework.game;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class PacMan implements GamingConsole {

    public void up(){
        System.out.println("PacMan please jump");
    }

    public void down(){
        System.out.println("PacMan please sleep");
    }

    public void left(){
        System.out.println("PacMan go left");
    }

    public void right(){
        System.out.println("PacMan go right");
    }
}
