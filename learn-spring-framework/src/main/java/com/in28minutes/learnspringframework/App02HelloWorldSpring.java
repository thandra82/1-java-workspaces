package com.in28minutes.learnspringframework;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class App02HelloWorldSpring {
    public static void main(String[] args) {

        // STEP-1 create spring application context
        var context = new AnnotationConfigApplicationContext(HelloWorldConfiguration.class);

        // STEP-2 retrieve the bean from configuration
        System.out.println(context.getBean("name"));

        System.out.println(context.getBean("age"));
        System.out.println(context.getBean("person"));

        System.out.println(context.getBean("address"));

        System.out.println("*****************************");
        System.out.println(Arrays.asList(context.getBeanDefinitionNames()));
        System.out.println("*****************************");

        Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);

    }
}
