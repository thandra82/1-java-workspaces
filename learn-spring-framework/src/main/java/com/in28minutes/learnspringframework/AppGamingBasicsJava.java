package com.in28minutes.learnspringframework;

import com.in28minutes.learnspringframework.game.GameRunner;
import com.in28minutes.learnspringframework.game.MarioGame;
import com.in28minutes.learnspringframework.game.PacMan;
import com.in28minutes.learnspringframework.game.SuperContra;

public class AppGamingBasicsJava {
    public static void main(String[] args) {

        // var marioGame = new MarioGame();
        var superContra = new SuperContra();
        var pacMan = new PacMan();
        
        GameRunner gameRunner = new GameRunner(superContra);
        gameRunner.run();
    }
}
