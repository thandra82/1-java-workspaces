package com.in28minutes.learnspringframework;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


record Person(String name,int age){};

record Address(String street1,String street2,String city, String zipCode) {};

@Configuration
public class HelloWorldConfiguration {

    @Bean
    public String name(){
        return "Srini";
    }

    @Bean
    public int age(){
        return 42;
    }

    @Bean
    public Person person(){
        var person = new Person("Shreyus",7);
        return person;
    }

    @Bean
    public Address address(){
        var address = new Address("3041","Sunfish Street","Prosper","75078");
        return address;
    }

}
