package com.in28minutes.springboot.learnspringboot;

import com.in28minutes.springboot.learnspringboot.pojos.Course;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class CourseController {

    @RequestMapping(path="/courses",method = RequestMethod.GET)
    public List<Course> retrieveAllCourses(){
        return Arrays.asList(new Course(1,"Learn AWS","In28minutes"),
                new Course(2, "Learn GCP in 2 days", "CourseEra"));
    }

}
