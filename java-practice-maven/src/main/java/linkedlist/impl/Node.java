package linkedlist.impl;

public class Node<E> {
    Object data;
    Node<E> next;

    Node(E d) {
        this.data = d;
        this.next = null;
    }
}
