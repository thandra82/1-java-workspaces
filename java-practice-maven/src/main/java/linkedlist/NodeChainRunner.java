package linkedlist;

public class NodeChainRunner {

    public static void main(String[] args) {

        Node firstNode = new Node(10);

        Node middleNode = new Node(20);

        firstNode.setNext(middleNode);

        Node lastNode = new Node(30);
        middleNode.setNext(lastNode);

        printNodes(firstNode);
    }

    public static void printNodes(Node node){

        while(node!=null){
            System.out.println(node.getValue());
            if(node.getNext()!=null) {
                System.out.println(node.getNext().getValue());
            }else{
                System.out.println(node.getNext());
            }
            System.out.println("**************************");

            node = node.getNext();

        }

    }
}
