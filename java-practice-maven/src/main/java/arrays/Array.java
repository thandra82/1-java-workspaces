package arrays;

import java.util.Scanner;

public class Array {

    public int[] getInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter No: of Elements ::");
        int n = scanner.nextInt();
        System.out.println("Enter elements:");

        int[] intArray = new int[n];
        for (int i = 0; i < n; i++) {
            intArray[i] = scanner.nextInt();
        }

        return intArray;
    }

    public void printArray(int[] array) {
        System.out.println("****** Final Output *************");
        for (int element : array) {
            System.out.println(element);
        }
        System.out.println("****** Final Output *************");
    }
}
