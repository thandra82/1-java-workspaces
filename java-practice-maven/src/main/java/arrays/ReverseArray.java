package arrays;

public class ReverseArray {


    public static void main(String[] args) {

        int[] testArray = new Array().getInput();

        ReverseArray reverseArray = new ReverseArray();
        int[] reversedArray = reverseArray.reverseArrayWithExtraArray(testArray);
        new Array().printArray(reversedArray);

    }

    public int[] reverseArrayWithExtraArray(int[] testArray) {
        System.out.println("-----------------------------");
        int[] reversedArray = new int[testArray.length];
        int j = 0;
        for (int i = testArray.length - 1; i >= 0; i--) {
            reversedArray[j] = testArray[i];
            j++;
        }

        return reversedArray;
    }

}
