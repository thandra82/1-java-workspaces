package arrays;

import org.junit.Test;


import static org.junit.Assert.assertArrayEquals;


public class ReverseArrayTest {

    @Test
    public void testReverseArray() {
        ReverseArray testReverseArray = new ReverseArray();
        int[] reversedResult = testReverseArray.reverseArrayWithExtraArray(new int[]{2, 3, 4});
        assertArrayEquals(reversedResult, new int[]{4, 3, 2});
    }
}
