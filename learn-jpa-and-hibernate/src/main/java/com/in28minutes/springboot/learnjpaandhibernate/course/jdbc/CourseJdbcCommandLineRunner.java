package com.in28minutes.springboot.learnjpaandhibernate.course.jdbc;

import com.in28minutes.springboot.learnjpaandhibernate.course.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CourseJdbcCommandLineRunner implements CommandLineRunner {

    @Autowired
    private CourseJdbcRepository courseJdbcRepository;

    @Override
    public void run(String... args) throws Exception {
        courseJdbcRepository.insert(new Course(1,"Learn AWS","AWS Cloud"));
        courseJdbcRepository.insert(new Course(2,"Learn Java","Java"));
        courseJdbcRepository.insert(new Course(3,"Learn NodeJS","Google"));

        courseJdbcRepository.deleteById(3);

        System.out.println(courseJdbcRepository.findById(2));
//        try{
//            Course newCourse = courseJdbcRepository.findById(3);
//        }catch (Exception e){
//            System.out.println("course not found");
//        }
    }
}
