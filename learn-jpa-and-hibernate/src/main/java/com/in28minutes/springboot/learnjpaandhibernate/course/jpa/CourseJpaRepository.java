package com.in28minutes.springboot.learnjpaandhibernate.course.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class CourseJpaRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public void insert(Coursejpa courseJPA){
        entityManager.merge(courseJPA);
    }

    public Coursejpa findById(long id){
        return entityManager.find(Coursejpa.class,id);
    }

    public void deleteById(long id){
        Coursejpa courseJPA = entityManager.find(Coursejpa.class,id);
        entityManager.remove(courseJPA);
    }
}
