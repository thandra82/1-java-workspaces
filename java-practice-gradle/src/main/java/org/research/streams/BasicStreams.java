package org.research.streams;

import org.research.objects.Cat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BasicStreams {

    public static void main(String[] args) {

        List<Cat> catsList = new ArrayList<>();

        catsList.add(new Cat("Freddy", 5));
        catsList.add(new Cat("Teddy", 3));
        catsList.add(new Cat("Cutie", 7));
        catsList.add(new Cat("Fido", 6));
        catsList.add(new Cat("Cramer", 9));

        List<Cat> olderCats = new ArrayList<>();

        olderCats = catsList.stream()
                            .filter(cat -> cat.getAge() >= 5)
                            .collect(Collectors.toList());

        System.out.println(olderCats);

        List<Integer> catAges = new ArrayList<>();

        catAges = catsList.stream()
                .map(cat-> cat.getAge())
                .collect(Collectors.toList());

        System.out.printf("Ages of All Cats::%s", Arrays.toString(catAges.toArray()));

    }
}
