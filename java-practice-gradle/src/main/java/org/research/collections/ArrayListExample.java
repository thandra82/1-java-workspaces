package org.research.collections;

import java.util.ArrayList;
import java.util.List;

public class ArrayListExample {

    public static void main(String[] args) {

        List<String> listOfNames =  new ArrayList<>();

        listOfNames.add("Srini");
        listOfNames.add("Priya");
        listOfNames.add("Shreyus");

        System.out.println(listOfNames);
    }
}
