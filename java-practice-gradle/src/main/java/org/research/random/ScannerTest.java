package org.research.random;

import java.util.Scanner;

public class ScannerTest {

    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);

        System.out.println("Please enter your Name:  ");
        String name = userInput.nextLine();

        System.out.println("Name is ::" + name);
    }

}
