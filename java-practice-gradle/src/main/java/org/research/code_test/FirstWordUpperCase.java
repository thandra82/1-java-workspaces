package org.research.code_test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FirstWordUpperCase {
    public static void main(String[] args) {
        System.out.println(returnCapsConvertedSTring("Hello this is Srini and how are you?"));
    }

    static String returnCapsConvertedSTring(String input){

        if (input == null || input.isEmpty()) {
            return null;
        }


        return Arrays.stream(input.split("\\s+"))
                .map(word -> {
                    System.out.println("The word is ->  "+ word);
                    return Character.toUpperCase(word.charAt(0)) + word.substring(1);})
                .collect(Collectors.joining(" "));
    }

    static List<String>  returnWordsSplitted(String input){

        if (input == null || input.isEmpty()) {
            return null;
        }


        return Arrays.stream(input.split("\\s+")).collect(Collectors.toList());
    }

}
