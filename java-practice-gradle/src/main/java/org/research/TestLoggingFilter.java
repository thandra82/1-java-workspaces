package org.research;

import java.util.logging.Filter;
import java.util.logging.LogRecord;

public class TestLoggingFilter implements Filter {

	@Override
	public boolean isLoggable(LogRecord record) {
		
		if(record.getMessage().contains("$")) {
			return false;
		}
		return true;
	}

}
