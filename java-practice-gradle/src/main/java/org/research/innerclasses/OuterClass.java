package org.research.innerclasses;

public class OuterClass {
    int x=10;

    class InnerClass{
        int y=20;

        void innerDisplay(){
            System.out.println(String.format("Outer Variable:: %d",x));
            System.out.println(String.format("Inner Variable:: %d",y));
        }
    }

    void outerDisplay(){
        InnerClass inner = new InnerClass();
        inner.innerDisplay();
        System.out.println(String.format("Inner Variable:: %d",inner.y));
    }

}
