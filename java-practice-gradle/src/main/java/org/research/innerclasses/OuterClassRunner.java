package org.research.innerclasses;

public class OuterClassRunner {
    public static void main(String[] args) {
        OuterClass outer = new OuterClass();

        outer.outerDisplay();
    }
}
