package org.research;

import java.util.Arrays;

public class RunningSumOfArray {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(runningSum(new int[]{1,5,7,4,6,12,10})));
        System.out.println(Arrays.toString(runningSumWithInnerLoop(new int[]{1,5,7,4,6,12,10})));

    }

    public static int[] runningSum(int[] originalArray){

        int[] resultArray = new int[originalArray.length];

        // First result in result array is same as original element
        resultArray[0] = originalArray[0];

        // start from second element
        for (int i = 1; i < originalArray.length ; i++) {
            resultArray[i] = originalArray[i] + resultArray[i-1];
        }

        return resultArray;
    }

    public static int[] runningSumWithInnerLoop(int[] originalArray){
        int[] resultArray = new int[originalArray.length];
        int sum=0;

        for (int i = 0; i < originalArray.length; i++) {

            for (int j = 0; j <= i; j++) {
                sum = sum + originalArray[j];
                resultArray[i] = sum;
            }
            sum=0;
        }


        return resultArray;
    }

}
