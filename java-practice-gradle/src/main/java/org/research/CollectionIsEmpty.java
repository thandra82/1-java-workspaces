package org.research;

import java.util.List;

public class CollectionIsEmpty {
    public static void main(String[] args) {
        System.out.println("START - ********************");
        List<Integer> list1 = List.of();

        System.out.println(list1.isEmpty());

        List<Integer> list2 = List.of(1,2,3,4,5,6,7,8,9,11,11,11,22,11,2242,2442,46546,56563,343434);

        System.out.println(list2.isEmpty());
        System.out.println("END - ********************");
    }
}
