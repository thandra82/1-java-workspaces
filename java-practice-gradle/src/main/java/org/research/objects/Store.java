package org.research.objects;

public class Store {

    Member members[] = new Member[100];
    private int count=0;

    public void register(Member member){
        members[count++] = member;
    }

    void inviteSalesDiscounts(){
        for (int i = 0; i < count; i++) {
            members[i].callback();
        }
    }
}
