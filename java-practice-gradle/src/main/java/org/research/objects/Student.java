package org.research.objects;

public class Student {
    private int rollNumber;
    private String name;
    private String courseName;
    private int marks1;
    private int marks2;
    private int marks3;

    public Student(int rollNumber, String name, String courseName, int marks1, int marks2, int marks3) {
        this.rollNumber = rollNumber;
        this.name = name;
        this.courseName = courseName;
        this.marks1 = marks1;
        this.marks2 = marks2;
        this.marks3 = marks3;
    }

    public int getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(int rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getMarks1() {
        return marks1;
    }

    public void setMarks1(int marks1) {
        this.marks1 = marks1;
    }

    public int getMarks2() {
        return marks2;
    }

    public void setMarks2(int marks2) {
        this.marks2 = marks2;
    }

    public int getMarks3() {
        return marks3;
    }

    public void setMarks3(int marks3) {
        this.marks3 = marks3;
    }

    public int totalMarks() {
        return marks1 + marks2 + marks3;
    }

    public double average() {
        return (marks1 + marks2 + marks3) / 3;
    }

    public char getGrade() {
        if (average() >= 70) {
            return 'A';
        } else {
            return 'B';
        }
    }

}
