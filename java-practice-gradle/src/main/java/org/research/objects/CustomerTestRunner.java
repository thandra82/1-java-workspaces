package org.research.objects;

import java.awt.*;

public class CustomerTestRunner {

    public static void main(String[] args) {

        Store store = new Store();

        Customer customer1 =  new Customer("Srini");
        Customer customer2 = new Customer("Shreyus");

        store.register(customer1);
        store.register(customer2);

        store.inviteSalesDiscounts();

    }
}
