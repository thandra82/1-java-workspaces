package org.research.objects;

public class Customer implements  Member{
    private String name;

    public Customer(String name) {
        this.name = name;
    }

    @Override
    public void callback() {
        System.out.printf("OK, I will visit the Store. This is ::=> %s",this.name);
        System.out.println();
    }
}
