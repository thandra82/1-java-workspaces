package org.research.objects;

public class Circle {
    private double radius;

    public Circle(){

    }

    public Circle(double area) {
        this.radius = area;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double area(){
        return Math.PI * radius * radius;
    }

    public double perimeter(){
        return 2 * Math.PI * radius;
    }

    public double circumference(){
        return 2 * Math.PI * radius;
    }

}
