package org.research.objects.runners;

import org.research.objects.Circle;

public class CircleRunner {
    public static void main(String[] args) {
        Circle simpleCircle = new Circle(2.0);
        System.out.printf("The Area of the circle is :: %f", simpleCircle.area());
    }
}
