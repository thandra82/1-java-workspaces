package org.research;

import java.util.logging.Filter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestLoggingFilterExample {
	
	 private static Logger logger = Logger.getLogger(TestLoggingFilterExample.class.getName());
	
	public static void main(String args[]) {
		
		Filter  testFilter = new TestLoggingFilter();
		logger.setFilter(testFilter);
		
		logger.log(Level.INFO,"Hello I am here-1");
		logger.log(Level.INFO,"Hello I am here i have $ amount");
		logger.log(Level.INFO,"Hello I am here i have $ amount");
		logger.log(Level.INFO,"Hello I am here-2");
		System.out.println("This came here working fine");
		
	}

}
