package com.in28minutes.objectsclasses;

public class MotorBikeRunner {
    public static void main(String[] args) {
        MotorBike ducati = new MotorBike();
        MotorBike honda = new MotorBike();

        ducati.start();
        honda.start();

        ducati.setSpeed(Short.valueOf("100"));
        System.out.println(String.format("The Speed of Ducati is :: %d km/hr",ducati.getSpeed()));

        honda.setSpeed(Short.valueOf("80"));
        System.out.println(String.format("The Speed of Honda is :: %d km/hr",honda.getSpeed()));


    }
}
