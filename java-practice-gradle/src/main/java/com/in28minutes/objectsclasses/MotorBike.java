package com.in28minutes.objectsclasses;

public class MotorBike {

    private short speed;

    public short getSpeed() {
        return this.speed;
    }

    public void setSpeed(short speed) {
        if (speed > 0)
            this.speed = speed;
    }

    public void start() {
        System.out.println("Bike Started");
    }
}
