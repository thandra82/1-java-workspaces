package com.functional_programming;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.util.stream.Collectors.groupingBy;

public class BookRunner {

    public static void main(String[] args) {
        Book book1 = new Book(UUID.randomUUID(),"Java Programming","Technical","Srini");
        Book book2 = new Book(UUID.randomUUID(),"Java Enterprise Programming","Technical","Srini");
        Book book3 = new Book(UUID.randomUUID(),"JavaScript","Technical","Shreyus");
        Book book4 = new Book(UUID.randomUUID(),"Shoe Dog","Philosophical","Phil Knight");
        Book book5 = new Book(UUID.randomUUID(),"A-Z Store","Business","Jeff");
        Book book6 = new Book(UUID.randomUUID(),"Trillion Dollar Company","Business","Jeff");

        List<Book> booksList= List.of(book1,book2,book3,book4,book5,book6);

         booksList.stream().forEach((book)->{
            System.out.println(book.getCategory());
        });

         Map<String, List<Book>> booksByCategory= booksList.parallelStream().collect(groupingBy(Book::getCategory));

        System.out.printf("booksList %s",booksByCategory);
        System.out.println(booksByCategory.size());

    }
}
