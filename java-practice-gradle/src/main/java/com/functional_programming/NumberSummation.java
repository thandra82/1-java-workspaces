package com.functional_programming;

import java.util.stream.IntStream;

public class NumberSummation {
    public static void main(String[] args) {

        /**
         * Imperative
         */

        int sum =0;
        for (int i = 0; i <= 100; i++) {
            sum =sum + i ;
        }

        System.out.println("sum = " + sum);

        /**
         * Declarative
         */

        int declarativeSum = IntStream.rangeClosed(0,100).sum();
        System.out.println("declarativeSum = " + declarativeSum);
    }
}
