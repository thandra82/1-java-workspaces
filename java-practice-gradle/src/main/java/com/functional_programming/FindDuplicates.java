package com.functional_programming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FindDuplicates {
    public static void main(String[] args) {

        List<Integer> lstNumbers = Arrays.asList(1, 2, 3, 3, 4, 5, 6, 6, 7, 9, 8);

        /**
         * Imperative
         */
        List<Integer> uniqueList = new ArrayList<>();
        for (Integer integer : lstNumbers) {
            if (!uniqueList.contains(integer)) {
                uniqueList.add(integer);
            }
        }
        System.out.printf("uniqueList %s",uniqueList);
        System.out.println();

        /**
         * Declarative Style
         */
        List<Integer> declarativeUniqueList =  lstNumbers.stream().distinct().collect(Collectors.toList());
        System.out.println("declarativeUniqueList:: " + declarativeUniqueList);

    }
}
