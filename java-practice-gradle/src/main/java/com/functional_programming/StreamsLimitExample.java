package com.functional_programming;

import java.net.Inet4Address;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class StreamsLimitExample {

    static Optional<Integer> limit(List<Integer> numbers){

        Stream<Integer> sumOfLimitedNumbers = numbers
                .stream()
                .peek((number)->{
                    System.out.println("**************");
                    System.out.println(number);
                    System.out.println("**************");
                }
                );
//                .limit(2)
//                .peek((number)-> System.out.println("number peeked:: "+ number))
//                .reduce(Integer::sum);

        return Integer.valueOf(0).describeConstable();
    }

    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(6,7,8,9,10);

        Optional<Integer> sumOfLimitedNumbers = limit(integers);

        if(sumOfLimitedNumbers.isPresent()){
            System.out.println("The sum of limited numbers is ::"+ sumOfLimitedNumbers.get());
        }else{
            System.out.println("Nothing present");
        }

    }
}
