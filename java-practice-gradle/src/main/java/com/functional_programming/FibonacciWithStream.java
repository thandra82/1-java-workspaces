package com.functional_programming;

import java.util.Arrays;
import java.util.stream.Stream;

public class FibonacciWithStream {

    public static void main(String[] args) {

        Stream.iterate(new int[] {0,1},x->new int[]{x[1],x[0]+x[1]})
                //.peek(x->System.out.println(x))
                .limit(8)
//                .flatMap(x->x[0])
                .forEach(x-> System.out.println(Arrays.toString(x)));
                //.forEach(x-> System.out.println(x[0]));
    }
}
