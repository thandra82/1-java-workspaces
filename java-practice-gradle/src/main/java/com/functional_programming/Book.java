package com.functional_programming;

import java.util.UUID;

public class Book {
    private UUID id;
    private String name;
    private String category;
    private String author;

    public Book(UUID id, String name, String category, String author) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
