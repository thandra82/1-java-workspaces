import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SayHello {

    private static Logger logger = Logger.getLogger(SayHello.class.getName());


    public static void main(String[] args) throws IOException {
        logger.addHandler(new ConsoleHandler());
        logger.setLevel(Level.ALL);
        // logger.entering(SayHello.class.getName(), "doIt");
        logger.log(Level.INFO,"Hello I am here");
        String language = args[0];

        InputStream resourceStream = SayHello.class.getClassLoader
                ().getResourceAsStream(language + ".txt");

        assert resourceStream != null;
        BufferedReader bufferedInputStream = new BufferedReader(new InputStreamReader
                (resourceStream, StandardCharsets.UTF_8));
        System.out.println(bufferedInputStream.readLine());
        // logger.exiting(SayHello.class.getName(), "doIt");

    }
}
