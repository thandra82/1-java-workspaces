import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.research.objects.User;


import static org.hamcrest.MatcherAssert.assertThat;

public class UserTest {

    @Test
    public void whenValuesAreNotInitialized_thenUserNameAndIdReturnDefault() {
        User user = new User();
        Assertions.assertNotNull(user);
        Assertions.assertNull(user.getName());
    }

}
